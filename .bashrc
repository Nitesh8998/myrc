# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@Researcher\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]   \[\033[01;33m\] $(parse_git_branch)\[\033[00m\]\n$ '
else
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w  $(parse_git_branch)\n$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
#alias ll='ls -alF'
#alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
#PS1='[\u@\h \W]\$ '

# for all the scripts of my scipts
source ~/utilScripts/.my_custom_cmds.sh
source ~/utilScripts/acd_func.sh
#source ~/utilScripts/set_date.sh
#source /tools/setup.sh
alias python=python3
#alias nfstools='sudo mount nfstools:/home/rise/tools /tools'

#alias nonfstools ='sudo umount /tools'
#IITM
#export LM_LICENSE_FILE=28...@10.21.226.183

#RISE
export LM_LICENS_FILE=27...@192.168.1.8
#export PATH=$PATH:/usr/bin/riscv/bin
#export ONI_NEOVIM_PATH=/home/nitesh/nvim.appimage

export NVIM=$HOME/.config/nvim
alias vim='nvim'

export  PATH=$PATH:/home/nitesh/temp/kitty/kitty/launcher
export PATH=/home/nitesh/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/nitesh/temp/kitty/kitty/launcher:~/.local/:/home/nitesh/.cargo/bin:/home/nitesh/.config/exercism


export PATH=$PATH:/home/nitesh/other_compilers/ispc-v1.11.0-linux/bin
export PATH=$PATH:/home/nitesh/dotnet/ARMSim-201
#export BLUESPEC_HOME=~/RISE/Bluespec_training/Bluespec-2017.07.A
#export BLUESPECDIR=$BLUESPEC_HOME/lib
#export PATH=$PATH:$BLUESPEC_HOME/bin
#export PATH=/home/nitesh/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/nitesh/temp/kitty/kitty/launcher:/home/nitesh/.local/:/home/nitesh/RISE/Bluespec_training/Bluespec-2017.07.A/bin:/home/nitesh/.cargo/bin


if [ -f ~/.config/exercism/exercism_completion.bash ]; then
      source ~/.config/exercism/exercism_completion.bash
fi



#export PYTHONPATH=$PYTHONPATH:/usr/lib/python3/dist-packages/
#export PATH=/root/.pyenv/bin:/home/nitesh/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/nitesh/temp/kitty/kitty/launcher:/home/nitesh/.local/:/home/nitesh/.cargo/bin:/home/nitesh/.config/exercism 
#export PATH="/home/nitesh/.pyenv/bin:$PATH"
#eval "$(pyenv init -)" 
#eval "$(pyenv virtualenv-init -)"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash


export TEXINPUTS=$TEXINPUTS:$HOME/share/latex/

alias python=python3.6 


#export LD_LIBRARY_PATH=/home/nitesh/other_compilers/cilkplus-4_8-install.tar_0/cilkplus-4_8-install/lib:/home/nitesh/other_compilers/cilkplus-4_8-install.tar_0/cilkplus-4_8-install/lib64:$LD_LIBRARY_PATH

export LM_LICENSE_FILE=2100@vayu:$LM_LICENSE_FILE
export XILINXD_LICENSE_FILE=2100@vayu
export PATH=$PATH:/home/nitesh/other_compilers/julia/

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/nitesh/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/nitesh/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/nitesh/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/nitesh/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

