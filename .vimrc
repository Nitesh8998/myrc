set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub


"One Dark
Plugin 'joshdick/onedark.vim'

Plugin 'git://git.wincent.com/command-t.git'

"grubBox
Plugin 'morhetz/gruvbox'

"Dracula
Plugin 'dracula/vim'

"Ayu THeme
Plugin 'ayu-theme/ayu-vim'

"Dracula Theme

"For indent Line
Plugin 'Yggdroot/indentLine'

" Plugin to help Fold
Plugin 'tmhedberg/SimpylFold'

" Auto Indentation
Plugin 'vim-scripts/indentpython.vim'

" Auto-Complete
Bundle 'Valloric/YouCompleteMe'

" Syntax Checking
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'

"Color Setting
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'

"File Browsing
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'

"Search Anything from vim Ctrl+P
Plugin 'kien/ctrlp.vim'

" Git Integration
Plugin 'tpope/vim-fugitive'

" Power Line
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

"multiple lines
Plugin 'https://github.com/terryma/vim-multiple-cursors'

"Shell Commands
Plugin 'https://github.com/tpope/vim-eunuch'

"Surround the Thing
Plugin 'https://github.com/tpope/vim-surround'

"Asynchronous Lint Engine :ALEDetail
Plugin 'https://github.com/w0rp/ale'

"Active Lines changes
Plugin 'https://github.com/airblade/vim-gitgutter'


"Nerd Commentor
Plugin 'scrooloose/nerdcommenter'


" DONT INSTALL THIS !!Plugin 'klen/python-mode'                   " Python mode (docs, refactor, lints...)


Plugin 'https://github.com/vim-scripts/vim-auto-save.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}



" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
let g:auto_save = 1
set nu
let g:SimplyFold_docstring_preview=1
syntax on
let g:auto_save_in_insert_mode = 0



" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
      \| exe "normal! g'\"" | endif
endif
" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif
set noswapfile
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab " this will replace every tab with spaces
set nu
set hlsearch
set incsearch
set autoindent
set wildmode=list:longest
set wildmenu                "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif
set colorcolumn=100
set tw=100
set wrap linebreak nolist

au BufReadPost,BufNewFile *.py set colorcolumn=80 tw=80

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>
nnoremap ` <CR>:Vexplore<CR>
autocmd BufNewFile *.bsv
\ 0r ~/.vim/lic.txt 

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
augroup ProjectDrawer
  autocmd!
"  autocmd VimEnter * :Vexplore
augroup END


"Enable Folding
set foldmethod=indent
set foldlevel=99

command Q q
"Enable folding with spacebar
nnoremap <space> za

" for python indentation
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 

highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

set encoding=utf-8

"You Complete me = \g
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_global_ycm_extra_conf='~/.vim/ycm_extra_conf.py'
let g:ycm_confirm_extra_conf=0
nmap <leader>g :YcmCompleter GoTo<CR>
nmap <leader>d :YcmCompleter GoToDefinition<CR>

"python with virtualenv support
py3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF

let python_highlight_all=1

"For theme
"if has('gui_running')
  "set background=dark  
 " colorscheme dracula
 " colorscheme solarized
"else
 " set background=dark
  "colorscheme dracula
 " colorscheme zenburn
"endif

call togglebg#map("<F5>")

"...
set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme

"colorscheme ayu

colorscheme gruvbox


function! ToggleTheme()
  "color ayu
   colorscheme dracula
  endfunction

"call ToggleTheme#map("<F7>")
"
"=====================================================
"" NERDTree settings
"=====================================================
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree
let NERDTreeWinSize=40
autocmd VimEnter * if !argc() | NERDTree | endif  " Load NERDTree only if vim is run without arguments

"System ClipBoard
set clipboard=unnamed

" syntastic
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list=1
let g:syntastic_enable_signs=1
let g:syntastic_check_on_wq=0
let g:syntastic_aggregate_errors=1
let g:syntastic_loc_list_height=5
let g:syntastic_error_symbol='X'
let g:syntastic_style_error_symbol='X'
let g:syntastic_warning_symbol='x'
let g:syntastic_style_warning_symbol='x'
let g:syntastic_python_checkers=['flake8', 'pydocstyle', 'python']





" Set this in input rc for vi like experience in shell
" set editing-mode vi
"
"

"Indent Line conceal colors
let g:indentLine_setColors = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

"NERD COMMENTOR
filetype plugin on


if has("gui_running")
  set guifont=Fira\ Code\ Medium\ 10
endif

let g:powerline_pycmd='py3'
